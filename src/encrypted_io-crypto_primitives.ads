with Ada.Streams;            use Ada.Streams;

--
--  This package provides the "cryptographic primitives" used by Core.
--  This package is just a very thin layer over a "true" cryptographic
--  kernel (SPARKNaCl). In case of changes in the cryptographic kernel
--  it suffices to update the private part and the core, leaving the
--  public part untouched.
--
private with SPARKNaCl.Core;      --  Note the "private" granting that no
private with SPARKNaCl.Hashing;   --  part of SPARKNaCl is used in the
private with SPARKNaCl.Stream;    --  public part

package Encrypted_Io.Crypto_Primitives is
   --  Result of hash function
   type Hash_Digest is private;

   --  Nonce used in the encryption/decryption procedures
   type Encryption_Nonce is private;

   --  Cryptographic key
   type Key_Type is limited private;

   Key_Bit_Size : constant Positive := 256;

   Key_Stream_Size : constant Stream_Element_Offset :=
                       Stream_Element_Offset (Key_Bit_Size / Stream_Element'Size);

   subtype Key_Array is Stream_Element_Array (1 .. Key_Stream_Size);

   --  Create a cryptographic key
   function To_Key (Data : Key_Array) return Key_Type;


   --
   --  Fill the vector with random data.  Used to create nonces, therefore
   --  it is not strictly necessary it is of perfect cryptographic quality
   --  (better if it is)
   --
   procedure Random_Fill (Item : out Stream_Element_Array);

   --
   --  Generate a random nonce for encryption. Again, strict cryptographic
   --  quality is not necessary.
   --
   function Random_Nonce return Encryption_Nonce;

   --
   --  The hash function provided takes the digest computed so far and
   --  it updates it with a new block of data.  An initial digest
   --  value is needed; here it is.
   --
   function Initial_Digest return Hash_Digest;

   --
   --  Update the digest with a new block of data
   --
   procedure Do_Hash (Digest : in out Hash_Digest;
                      Item   : in Stream_Element_Array);

   --
   --  Convert an hash digest to a Stream_Element_Array
   --
   function To_Stream_Array (Digest : Hash_Digest) return Stream_Element_Array;

   --
   --  Decrypt an encrypted text using the provided Key and Nonce
   --
   procedure Decrypt (Encrypted  : Stream_Element_Array;
                      Clear_Text : out Stream_Element_Array;
                      Key        : Key_Type;
                      Nonce      : Encryption_Nonce);

   --
   --  Encrypt a clear text using the provided Key and Nonce
   --
   procedure Encrypt (Encrypted  : out Stream_Element_Array;
                      Clear_Text : Stream_Element_Array;
                      Key        : Key_Type;
                      Nonce      : Encryption_Nonce);
private
   type Hash_Digest is
      record
         Hash : SPARKNaCl.Hashing.Digest;
      end record;

   type Encryption_Nonce is
      record
         Nonce : SPARKNaCl.Stream.Salsa20_Nonce;
      end record;

   type Key_Type  is
      record
         K : SPARKNaCl.Core.Salsa20_Key;
      end record;

   pragma Compile_Time_Error
     (SPARKNaCl.Core.Salsa20_Key'Size = Key_Bit_Size,
      "Key_Bit_Size does not match the actual key size");

   pragma Compile_Time_Error (0 = 1, "pippo");

end Encrypted_Io.Crypto_Primitives;

with Ada.Streams;

--
--  This package collects a bit of "odds and ends" functions that are
--  useful, but without a "connection" among them (e.g., conversion functions).
--  They are here just to avoid cluttering Core.
--
private package Encrypted_Io.Utilities is
   use Ada.Streams;

   function To_Stream_Array (X : String) return Stream_Element_Array;
end Encrypted_Io.Utilities;

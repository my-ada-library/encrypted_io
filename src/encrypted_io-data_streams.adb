pragma Ada_2022;
package body Encrypted_Io.Data_Streams is
   package Unbounded renames Ada.Streams.Storage.Unbounded;
   ----------
   -- Read --
   ----------

   overriding procedure Read
     (Stream : in out Stream_Type; Item : out Stream_Element_Array;
      Last   :    out Stream_Element_Offset)
   is
   begin
      Unbounded.Stream_Type (Stream).Read (Item, Last);
   end Read;

   -----------
   -- Write --
   -----------

   overriding procedure Write
     (Stream : in out Stream_Type; Item : Stream_Element_Array)
   is
   begin
      Unbounded.Stream_Type (Stream).Write (Item);
   end Write;

   -------------------
   -- Element_Count --
   -------------------

   function Element_Count (Stream : Stream_Type) return Stream_Element_Count is
   begin
      return Unbounded.Stream_Type (Stream).Element_Count;
   end Element_Count;

   -----------
   -- Clear --
   -----------

   procedure Clear (Stream : in out Stream_Type) is
   begin
      Unbounded.Stream_Type (Stream).Clear;
   end Clear;

end Encrypted_Io.Data_Streams;

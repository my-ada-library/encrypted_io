pragma Ada_2012;
with Ada.Unchecked_Conversion;
with Ada.Numerics.Discrete_Random;

with Encrypted_Io.Utilities;

package body Encrypted_Io.Crypto_Primitives is
   use SPARKNaCl;


   function To_Byte_Seq (X     : Stream_Element_Array;
                         First : I32 := 0) return Byte_Seq;

   function To_Stream_Array (X : Byte_Seq) return Stream_Element_Array;

   function To_Byte_Seq (X : String) return Byte_Seq
   is (To_Byte_Seq (Utilities.To_Stream_Array (X)));

   -----------------
   -- To_Byte_Seq --
   -----------------

   function To_Byte_Seq (X     : Stream_Element_Array;
                         First : I32 := 0) return Byte_Seq
   is
      use type I32;

      subtype Source is Stream_Element_Array (X'Range);
      subtype Target is Byte_Seq (First .. First + Source'Length - 1);

      function Convert is
        new Ada.Unchecked_Conversion (Source => Source,
                                      Target => Target);
   begin
      return Convert (X);
   end To_Byte_Seq;

   ---------------------
   -- To_Stream_Array --
   ---------------------

   function To_Stream_Array (X : Byte_Seq) return Stream_Element_Array
   is
      subtype Target is Stream_Element_Array (1 .. X'Length);
      subtype Source is Byte_Seq (1 .. X'Length);

      function Convert is
        new Ada.Unchecked_Conversion (Source => Source,
                                      Target => Target);
   begin
      return Convert (X);
   end To_Stream_Array;



   ------------
   -- To_Key --
   ------------

   function To_Key (Data : Key_Array)
                    return Key_Type is
      function To_Bytes32 is
        new Ada.Unchecked_Conversion (Source => Key_Array,
                                      Target => SPARKNaCl.Bytes_32);
   begin
      return (K => SPARKNaCl.Core.Construct (To_Bytes32 (Data)));
   end To_Key;

   -----------------
   -- Random_Fill --
   -----------------


   procedure Random_Fill (Item : out Stream_Element_Array)
   is
      package Random_Bytes is
        new Ada.Numerics.Discrete_Random (Stream_Element);

      Gen : Random_Bytes.Generator;
   begin
      Random_Bytes.Reset (Gen);

      for I in Item'Range loop
         Item (I) := Random_Bytes.Random (Gen);
      end loop;
   end Random_Fill;

   ------------------
   -- Random_Nonce --
   ------------------

   function Random_Nonce return Encryption_Nonce
   is
      use SPARKNaCl.Stream;

      Tmp : Stream_Element_Array (1 .. Salsa20_Nonce'Length);
   begin
      Random_Fill (Tmp);
      return (Nonce => Salsa20_Nonce (To_Byte_Seq (Tmp)));
   end Random_Nonce;

   --------------------
   -- Initial_Digest --
   --------------------

   function Initial_Digest return Hash_Digest
   is (Hash => SPARKNaCl.Hashing.IV);


   -------------
   -- Do_Hash --
   -------------

   procedure Do_Hash
     (Digest : in out Hash_Digest; Item : in Stream_Element_Array)
   is
   begin
      SPARKNaCl.Hashing.Hashblocks (X => Digest.Hash,
                                    M => To_Byte_Seq (Item));
   end Do_Hash;

   ---------------------
   -- To_Stream_Array --
   ---------------------

   function To_Stream_Array (Digest : Hash_Digest) return Stream_Element_Array
   is (To_Stream_Array (SPARKNaCl.Byte_Seq (Digest.Hash)));

   -------------
   -- Decrypt --
   -------------

   procedure Decrypt
     (Encrypted  : Stream_Element_Array;
      Clear_Text : out Stream_Element_Array;
      Key        : Key_Type;
      Nonce      : Encryption_Nonce)
   is
   begin
      --
      --  SparkNaCl encryption is stream-based: the cleartext is xor-ed with
      --  a bitstream obtained from the key.  This makes decryption the same
      --  as encryption, but with clear text and cypher text swapped.
      --
      Encrypt (Encrypted  => Clear_Text,
               Clear_Text => Encrypted,
               Key        => Key,
               Nonce      => Nonce);
   end Decrypt;

   -------------
   -- Encrypt --
   -------------

   procedure Encrypt
     (Encrypted  : out Stream_Element_Array;
      Clear_Text : Stream_Element_Array;
      Key        : Key_Type;
      Nonce      : Encryption_Nonce)
   is
      use SPARKNaCl;

      Src : constant Byte_Seq := To_Byte_Seq (Clear_Text);
      Dst : Byte_Seq (Src'Range);
   begin
      SPARKNaCl.Stream.Salsa20_Xor (C => Dst,
                                    M => Src,
                                    N => Nonce.Nonce,
                                    K => Key.K);

      Encrypted := To_Stream_Array (Dst);
   end Encrypt;
end Encrypted_Io.Crypto_Primitives;

with Ada.Streams.Stream_IO;    use Ada.Streams;

--
--  This package provides subroutines to load and save encrypted files. Every
--  type of subroutine (save or load) is provided in 6 "flavors" that
--  correspond to the combination of 2 "keying approaches" and 3 "data types."
--
--  More precisely, the 2 keying approaches are
--
--  1. Password-based.  The user provides a password that is internal
--     stretched in order to combat brute-force methods
--
--  2. Key based.  The user provides directly the key
--
--  The three data-type are
--
--  1. **Streams**  The load functions return a Stream that can be read with
--     the usual stream functions; save procedures accepts the data as
--     a stream that will be read to get the data to be encrypted
--
--  2. **Stream_Element_Array** The load function returns a
--     Stream_Element_Array and the save procedure expects one
--
--  3. **Generic** generic load/save function are provided.  They are
--     parametized by a data type and read/write a file containing a
--     value of the given data type.  Quite convinient if the data type
--     is an array
--
--  All the functions/procedures accept the name of the file to be read/write
--  as parameter; the only exception are a load function and a save procedure
--  that works with `Stream_IO.Stream_Type`.  These, actually, are functions
--  used internally, but we decided that they could be useful in some context.
--

with Encrypted_Io.Data_Streams;
with Encrypted_Io.Crypto_Primitives;


package Encrypted_Io.Core is
   type Stretching_Options is private;

   type Iteration_Count is mod 2 ** 64;

   --
   --  Stretching is usually done by iterating a function (e.g. an hash
   --  function) many times, in order to make the password creation less
   --  efficient.  The impact on a legitimate user is negligible, but it
   --  amplify the difficulty of a brute force attack.  The number of
   --  iteration to be used can be specified by using this function.
   --
   function N_Iterations (N : Iteration_Count) return Stretching_Options;

   --
   --  Default stretching options.  The default number of iterations is
   --  1_000_000
   --
   Default : constant Stretching_Options;

   type Password_Nonce is new Stream_Element_Array;

   function Random_Nonce return Password_Nonce;

   --
   --  Stretch a password together with the nonce to get a key.  This does
   --  not increase the entropy of the key, but it makes more difficult
   --  to use brute force attacks.
   --
   function Stretch_Password
     (Password : String;
      Nonce    : Password_Nonce;
      Options  : Stretching_Options := Default)
      return Crypto_Primitives.Key_Type;

   --
   --  Load a file that has been encrypted with the given key, return a
   --  Stream that can be used to read the data
   --
   function Load_Encrypted_File
     (Filename : String;
      Key      : Crypto_Primitives.Key_Type)
      return Data_Streams.Stream_Type;

   --
   --  Like the above function, but read all the data from the stream and
   --  return them in an array
   --
   function Load_Encrypted_File
     (Filename : String;
      Key      : Crypto_Primitives.Key_Type)
      return Stream_Element_Array;

   generic
      type Data_Type (<>) is private;
   function Generic_Load_With_Key (Filename : String;
                                   Key      : Crypto_Primitives.Key_Type)
                                   return Data_Type;

   --
   --  Load a file that has been encrypted with the given password, and
   --  return a Stream that can be used to read the data.  The password is
   --  stretched using a nonce stored in the file itself.
   --
   function Load_Encrypted_File
     (Filename : String;
      Password : String)
      return Data_Streams.Stream_Type;

   --
   --  Like the above function, but read all the data from the stream and
   --  return them in an array
   --
   function Load_Encrypted_File
     (Filename : String;
      Password : String)
      return Stream_Element_Array;

   generic
      type Data_Type (<>) is private;
   function Generic_Load_With_Password (Filename : String;
                                        Password : String)
                                        return Data_Type;

   --
   --  Load an encrypted file, but in this case the data are read from
   --  a Stream_IO stream.
   --
   function Load_Encrypted_File
     (Stream   : Stream_IO.Stream_Access;
      Size     : Stream_IO.Count;
      Key      : Crypto_Primitives.Key_Type)
      return Data_Streams.Stream_Type;


   --
   --  Like the above function, but read all the data from the stream and
   --  return them in an array
   --
   function Load_Encrypted_File
     (Stream   : Stream_IO.Stream_Access;
      Size     : Stream_IO.Count;
      Key      : Crypto_Primitives.Key_Type)
      return Stream_Element_Array;

   procedure Save_Encrypted_File
     (Filename : String;
      Key      : Crypto_Primitives.Key_Type;
      Data     : in out Data_Streams.Stream_Type);

   procedure Save_Encrypted_File
     (Filename : String;
      Password : String;
      Data     : in out Data_Streams.Stream_Type;
      Options  : Stretching_Options := Default);

   procedure Save_Encrypted
     (Stream : Stream_IO.Stream_Access;
      Key    : Crypto_Primitives.Key_Type;
      Data   : in out Data_Streams.Stream_Type);

   generic
      type Data_Type (<>) is private;
   procedure Generic_Save_With_Password
     (Filename : String;
      Password : String;
      Item     : Data_Type;
      Options  : Stretching_Options := Default);

   generic
      type Data_Type (<>) is private;
   procedure Generic_Save_With_Key (Filename : String;
                                    Key      : Crypto_Primitives.Key_Type;
                                    Item     : Data_Type);
private
   type Stretching_Options is
      record
         Iterations : Iteration_Count;
      end record;

   Default : constant Stretching_Options := (Iterations => 1_000_000);

   function N_Iterations (N : Iteration_Count) return Stretching_Options
   is ((Iterations => N));
end Encrypted_Io.Core;

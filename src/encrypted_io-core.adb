pragma Ada_2022;
with Ada.Text_IO; use Ada.Text_IO;
with Ada.Unchecked_Conversion;

--  with SPARKNaCl.Hashing;
--  with SPARKNaCl.Stream;

with Encrypted_Io.Utilities;
with Encrypted_Io.Crypto_Primitives;

package body Encrypted_Io.Core is

   Password_Nonce_Size : constant Stream_Element_Count := 512 / 8;

   ------------------
   -- Random_Nonce --
   ------------------

   function Random_Nonce  return Password_Nonce
   is
      Result : Password_Nonce (1 .. Password_Nonce_Size);
   begin
      Crypto_Primitives.Random_Fill (Stream_Element_Array (Result));

      return Result;
   end Random_Nonce;


   ----------------------
   -- Stretch_Password --
   ----------------------

   function Stretch_Password
     (Password : String;
      Nonce    : Password_Nonce;
      Options  : Stretching_Options := Default)
      return Crypto_Primitives.Key_Type
   is
      use Crypto_Primitives;

      Stretch_Input : constant Stream_Element_Array :=
                        Utilities.To_Stream_Array (Password)
                      & Stream_Element_Array (Nonce);

      Digest : Hash_Digest := Initial_Digest;
   begin
      for I in 1 .. Options.Iterations loop
         Do_Hash (Digest, Stretch_Input);
      end loop;

      declare
         Tmp : constant Stream_Element_Array := To_Stream_Array (Digest);
      begin
         return To_Key (Tmp (1 .. Key_Stream_Size));
      end;
   end Stretch_Password;




   -------------------------
   -- Load_Encrypted_File --
   -------------------------

   function Load_Encrypted_File
     (Stream   : Stream_IO.Stream_Access;
      Size     : Stream_IO.Count;
      Key      : Crypto_Primitives.Key_Type)
      return Data_Streams.Stream_Type
   is
      use Crypto_Primitives;

      Encrypted  : Stream_Element_Array (1 .. Stream_Element_Count (Size));
      Last       : Stream_Element_Offset;
      Nonce      : Encryption_Nonce;
   begin
      Encryption_Nonce'Read (Stream, Nonce);

      Read (Stream => Stream.all,
            Item   => Encrypted,
            Last   => Last);

      declare
         Clear_Text : Stream_Element_Array (1 .. Last);
      begin
         Crypto_Primitives.Decrypt (Encrypted  => Encrypted,
                                    Clear_Text => Clear_Text,
                                    Key        => Key,
                                    Nonce      => Nonce);

         --  SPARKNaCl.Stream.Salsa20_Xor (C => Dst,
         --                                M => Src,
         --                                N => Nonce,
         --                                K => Key.K);
         --
         --  Clear_Text := To_Stream_Array (Dst);

         return Result : Data_Streams.Stream_Type do
            Result.Write (Clear_Text);
         end return;
      end;
   end Load_Encrypted_File;

   -------------------------
   -- Load_Encrypted_File --
   -------------------------

   function Load_Encrypted_File (Filename : String;
                                 Key      : Crypto_Primitives.Key_Type)
                                 return Data_Streams.Stream_Type
   is
      Input : Stream_IO.File_Type;
   begin
      Stream_IO.Open (File => Input,
                      Mode => Stream_IO.In_File,
                      Name => Filename);

      return Result : constant Data_Streams.Stream_Type :=
        Load_Encrypted_File (Stream => Stream_IO.Stream (Input),
                             Size   => Stream_IO.Size (Input),
                             Key    => Key)
      do
         Stream_IO.Close (Input);
      end return;
   end Load_Encrypted_File;

   -------------------------
   -- Load_Encrypted_File --
   -------------------------

   function Load_Encrypted_File (Filename : String;
                                 Password : String)
                                 return Data_Streams.Stream_Type
   is
      Input   : Stream_IO.File_Type;
      Stream  : Stream_IO.Stream_Access;
      Options : Stretching_Options;
   begin
      Stream_IO.Open (File => Input,
                      Mode => Stream_IO.In_File,
                      Name => Filename);

      Stream := Stream_IO.Stream (Input);
      Stretching_Options'Read (Stream, Options);

      declare
         Nonce : constant Password_Nonce := Password_Nonce'Input (Stream);
         Key   : constant Crypto_Primitives.Key_Type :=
                   Stretch_Password (Password, Nonce, Options);
      begin
         return Result : constant Data_Streams.Stream_Type :=
           Load_Encrypted_File (Stream => Stream,
                                Size   => Stream_IO.Size (Input),
                                Key    => Key)
         do
            Stream_IO.Close (Input);
         end return;
      end;
   end Load_Encrypted_File;

   function Load_Encrypted_File
     (Filename : String;
      Key      : Crypto_Primitives.Key_Type)
      return Stream_Element_Array
   is
      Stream : Data_Streams.Stream_Type :=
                 Load_Encrypted_File (Filename, Key);

      Result : Stream_Element_Array (1 .. Stream.Element_Count);
      Last   : Stream_Element_Offset;
   begin
      Stream.Read (Result, Last);

      return Result;
   end Load_Encrypted_File;


   ------------------
   -- Generic_Load --
   ------------------

   function Generic_Load_With_Key (Filename : String;
                                   Key      : Crypto_Primitives.Key_Type)
                                   return Data_Type
   is
      S : aliased Data_Streams.Stream_Type :=
            Load_Encrypted_File (Filename, Key);
   begin
      return Data_Type'Input (S'Access);
   end Generic_Load_With_Key;

   function Load_Encrypted_File
     (Filename : String;
      Password : String)
      return Stream_Element_Array
   is
      Stream : Data_Streams.Stream_Type :=
                 Load_Encrypted_File (Filename, Password);

      Result : Stream_Element_Array (1 .. Stream.Element_Count);
      Last   : Stream_Element_Offset;
   begin
      Stream.Read (Result, Last);

      return Result;
   end Load_Encrypted_File;


   ------------------
   -- Generic_Load --
   ------------------

   function Generic_Load_With_Password (Filename : String;
                                        Password : String)
                                        return Data_Type
   is
      S : aliased Data_Streams.Stream_Type :=
            Load_Encrypted_File (Filename, Password);
   begin
      return Data_Type'Input (S'Access);
   end Generic_Load_With_Password;

   function Load_Encrypted_File
     (Stream   : Stream_IO.Stream_Access;
      Size     : Stream_IO.Count;
      Key      : Crypto_Primitives.Key_Type)
      return Stream_Element_Array
   is
      S : Data_Streams.Stream_Type :=
            Load_Encrypted_File (Stream, Size, Key);

      Result : Stream_Element_Array (1 .. S.Element_Count);
      Last   : Stream_Element_Offset;
   begin
      S.Read (Result, Last);

      return Result;
   end Load_Encrypted_File;



   --------------------
   -- Save_Encrypted --
   --------------------

   procedure Save_Encrypted
     (Stream : Stream_IO.Stream_Access;
      Key    : Crypto_Primitives.Key_Type;
      Data   : in out Data_Streams.Stream_Type)
   is
      Nonce  : constant Crypto_Primitives.Encryption_Nonce :=
                 Crypto_Primitives.Random_Nonce;

      Clear_Text : Stream_Element_Array (1 .. Data.Element_Count);
      Last       : Stream_Element_Offset;
   begin
      Data.Read (Item => Clear_Text,
                 Last => Last);

      pragma Assert (Clear_Text'Last = Last);

      declare
         Encrypted : Stream_Element_Array (Clear_Text'Range);
      begin
         Crypto_Primitives.Encrypt (Encrypted  => Encrypted,
                                    Clear_Text => Clear_Text,
                                    Key        => Key,
                                    Nonce      => Nonce);

         Crypto_Primitives.Encryption_Nonce'Output (Stream, Nonce);

         Stream.Write (Encrypted);
      end;
   end Save_Encrypted;

   -------------------------
   -- Save_Encrypted_File --
   -------------------------

   procedure Save_Encrypted_File
     (Filename : String;
      Key      : Crypto_Primitives.Key_Type;
      Data     : in out Data_Streams.Stream_Type)
   is
      Target : Stream_IO.File_Type;
   begin
      Stream_IO.Create (File => Target,
                        Mode => Stream_IO.Out_File,
                        Name => Filename);

      Save_Encrypted (Stream => Stream_IO.Stream (Target),
                      Key    => Key,
                      Data   => Data);

      Stream_IO.Close (Target);
   end Save_Encrypted_File;

   -------------------------
   -- Save_Encrypted_File --
   -------------------------

   procedure Save_Encrypted_File
     (Filename : String;
      Password : String;
      Data     : in out Data_Streams.Stream_Type;
      Options  : Stretching_Options := Default)
   is
      Nonce  : constant Password_Nonce := Random_Nonce;
      Key    : constant Crypto_Primitives.Key_Type :=
                 Stretch_Password (Password, Nonce, Options);
      Output : Stream_IO.File_Type;
      Stream : Stream_IO.Stream_Access;
   begin
      Stream_IO.Create (File => Output,
                        Mode => Stream_IO.Out_File,
                        Name => Filename);

      Stream := Stream_IO.Stream (Output);

      Stretching_Options'Write (Stream, Options);
      Password_Nonce'Output (Stream, Nonce);
      Save_Encrypted (Stream, Key, Data);

      Stream_IO.Close (Output);
   end Save_Encrypted_File;

   --------------------------------
   -- Generic_Save_With_Password --
   --------------------------------

   procedure Generic_Save_With_Password
     (Filename : String;
      Password : String;
      Item     : Data_Type;
      Options  : Stretching_Options := Default)
   is
      Serialized_Data : aliased Data_Streams.Stream_Type;
   begin
      Data_Type'Output (Serialized_Data'Access, Item);

      Save_Encrypted_File (Filename => Filename,
                           Password => Password,
                           Options  => Options,
                           Data     => Serialized_Data);
   end Generic_Save_With_Password;

   ---------------------------
   -- Generic_Save_With_Key --
   ---------------------------

   procedure Generic_Save_With_Key (Filename : String;
                                    Key      : Crypto_Primitives.Key_Type;
                                    Item     : Data_Type)
   is
      Serialized_Data : aliased Data_Streams.Stream_Type;
   begin
      Data_Type'Output (Serialized_Data'Access, Item);

      Save_Encrypted_File (Filename => Filename,
                           Key      => Key,
                           Data     => Serialized_Data);
   end Generic_Save_With_Key;

end Encrypted_Io.Core;

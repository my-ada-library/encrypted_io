pragma Ada_2012;
with Ada.Unchecked_Conversion;
with Ada.Numerics.Discrete_Random;

package body Encrypted_Io.Utilities is


   --

   --  function Get_Salsa20_Nonce return SPARKNaCl.Stream.Salsa20_Nonce
   --  is
   --     Result : SPARKNaCl.Stream.Salsa20_Nonce;
   --  begin
   --     Random_Fill (Byte_Seq (Result));
   --     return Result;
   --  end Get_Salsa20_Nonce;


   ---------------------
   -- To_Stream_Array --
   ---------------------

   function To_Stream_Array (X : String) return Stream_Element_Array
   is
      subtype Target is Stream_Element_Array (1 .. X'Length);
      subtype Source is String (1 .. X'Length);

      function Convert is
        new Ada.Unchecked_Conversion (Source => Source,
                                      Target => Target);
   begin
      return Convert (X);
   end To_Stream_Array;


end Encrypted_Io.Utilities;

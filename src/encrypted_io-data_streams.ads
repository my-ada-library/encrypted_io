pragma Ada_2022;

with Ada.Streams;  use Ada.Streams;

private with Ada.Streams.Storage.Unbounded;

--
--  This layer provides a thin layer over Ada.Streams.Storage.Unbounded
--  which has been introduced in Ada 2022.  A non-Ada 2022 version must
--  just retain the public interface and provide its own implementation
--  of a storage-based stream.
--

package Encrypted_Io.Data_Streams is
   type Stream_Type is
     new Ada.Streams.Root_Stream_Type
   with
     private;

   overriding procedure Read
     (Stream : in out Stream_Type;
      Item   : out Stream_Element_Array;
      Last   :    out Stream_Element_Offset);

   overriding procedure Write
     (Stream : in out Stream_Type; Item : Stream_Element_Array) ;

   function Element_Count (Stream : Stream_Type) return Stream_Element_Count;

   procedure Clear (Stream : in out Stream_Type);

private
   type Stream_Type is
     new Ada.Streams.Storage.Unbounded.Stream_Type
   with
     null record;
end Encrypted_Io.Data_Streams;

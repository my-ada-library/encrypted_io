# What is this?

This is a small Ada 202x library that allows one to read/write encrypted files using sparkNaCl (an Ada implementation of the NaCl suite verified with Spark/Ada).

The "cleartext" version of the file is presented as a `Storage` stream; this is the reason for requiring Ada 202x, since `Stream.Storage` has been introduced with Ada 202x.

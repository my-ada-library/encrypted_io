with SPARKNaCl.Core;
with Ada.Text_IO; use Ada.Text_IO;
with Encrypted_Io.Crypto_Primitives;
procedure zorro is
begin
   Put_Line (SPARKNaCl.Core.Salsa20_Key'Size'Image);
   Put_Line (Encrypted_Io.Crypto_Primitives.Key_Bit_Size'Image);
end zorro;

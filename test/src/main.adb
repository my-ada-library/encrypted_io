with Ada.Text_IO; use Ada.Text_IO;

with Encrypted_Io.Core;
with Encrypted_Io.Crypto_Primitives;

procedure Main is
   use Encrypted_Io;

   type Byte is mod 256;

   type Pixel is
      record
         X, Y    : Integer;
         R, G, B : Byte;
      end record;

   type Data_Array is array (Positive range <>) of Pixel;

   procedure Save is
     new Core.Generic_Save_With_Password (Data_Array);

   function Load is
     new Core.Generic_Load_With_Password (Data_Array);

   procedure Save is
     new Core.Generic_Save_With_Key (Data_Array);

   function Load is
     new Core.Generic_Load_With_Key (Data_Array);

   Data : constant Data_Array :=
            ((1, 2, 8, 9, 19),
             (3, 4, 0, 0, 255));

   Key  : constant Crypto_Primitives.Key_Type :=
            Crypto_Primitives.To_Key ((others => 16#A0#));

   procedure Print (X : Pixel) is
   begin
      Put_Line ("(" & X.X'Image & ", " & X.Y'Image & ")");
      Put_Line ("(" & X.R'Image & ", " & X.G'Image & ", " & X.B'Image & ")");
   end Print;
   pragma Unreferenced (Print);

begin
   Save ("rumenta/pippo", "zorro", Data, Core.N_Iterations (256));

   declare
      Q : constant Data_Array := Load ("rumenta/pippo", "zorro");
   begin
      Put_Line ((if Q = Data then "Success" else "Failure"));
   end;

   Save ("rumenta/pluto", Key, Data);

   declare
      Q : constant Data_Array := Load ("rumenta/pluto", Key);
   begin
      Put_Line ((if Q = Data then "Success" else "Failure"));
   end;
end Main;
